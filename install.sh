#!/bin/sh

opt=`echo $1 | tr -d '-'`
bin=gensite
case $opt in
	g|global) cp build.sh /usr/bin/$bin;;
	l|local) 
		cp build.sh $HOME/.local/bin/$bin;;
	*) 
cat <<EOF
Options:
   g|global : copy build.sh to /usr/bin/$bin
   l|local  : copy buils.dh to $HOME/.local/bin/$bin
EOF
exit 0
;;
esac
echo $bin Installed successfully
