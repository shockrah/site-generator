#!/bin/bash

# Why? becausee a simple markdown to html converter is required for those that 
# don't want to install pandoc that don't have it already

IFS=$'\n'
code=0
ul=0
paragraph() {
	# Replace ![](/file/path.png) with proper img tag links
	[ -z $1 ] && echo ''
	[ 1 = $code ] && echo $1 || \
	echo '<p>'$1'</p>' | sed -E 's/`(.*)`/<code>\1<\/code>/g;s/\!\[(.*)\]\((.*)\)/<img src="\2" alt="\1">/g;s/\[(.*)\]\((.*)\)/<a href="\2">\1<\/a>/g'
}

convert_links() {
	sed -E 's/\[(.*)\]\((.*)\)/<a href="\2">\1<\/a>/g' < /dev/stdin
}

header() {
	depth=$(echo $1 | grep '^##*' -o | tr -d '\n' | wc -m)
	line=${1:$depth}
	echo "<h$depth>$line</h$depth>" | convert_links
}

blockquote() {
	# remove starting > symbols before doing anything
	echo "<blockquote>${1:1}</blockquote>" | convert_links
}


fluff_trim() {
	perl -0777 -pe 's/<\/p>\n<p>//g;s/<p><\/p>//g' < /dev/stdin
}

code_chunk() {
	if [ 0 = $code ];then
		code=1
		echo '<pre><code>'
	else
		code=0
		echo '</pre></code>'
	fi
}

set_ul() {
	if [ 1 = $ul ];then
		ul=2
	fi
}

unordered_list() {
	case $ul in 
		0)echo "<ul><li>${1:1}</li>";ul=1;;
		1)echo "<li>${1:1}</li>";ul=1;;
		2)echo "<li>${1:1}</li></ul>";ul=0;;
	esac
}

if [ -z $1 ];then
	echo Usage: $0 file.md
	exit 0
fi

while read line;do
	case $line in
		\#*) set_ul; header $line;;
		\>*) set_ul; blockquote $line;;
		\`\`\`) set_ul; code_chunk;;
		\**) unordered_list $line;;
		# TODO: ordered_lists
		*) set_ul; paragraph "$line";;
	esac
done < "$1" | fluff_trim | grep -v '^$'
